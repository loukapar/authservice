FROM node:11-alpine

RUN mkdir -p /usr/src/app

WORKDIR /usr/src/app

COPY . .

RUN npm rebuild

# RUN npm install
RUN apk add --no-cache --virtual .gyp python make g++ && npm install && apk del .gyp

# RUN npm audit fix

RUN npm install pm2 -g

CMD [ "pm2-runtime", "npm", "--", "start" ]
# CMD ["npm", "run", "start"]


# ENTRYPOINT [ "entrypoint.sh" ]
