import fs from "fs";
import env from "../config/environment";
import DB from "../utils/database";
import { RegistrationMethods } from "../services/user.services/register.service";
import { UserDetail } from "../services/user.services/user.detail.service";

export default class InitializeDatabase {
  static async initializeRoles() {
    try {
      let rawdata: any = fs.readFileSync(`${env.BASE_DIR}/dbInitialization/data/roles.json`);
      let roles = JSON.parse(rawdata);

      if ((await DB.Models.Role.find().countDocuments()) > 0) {
        console.log("Roles already contain values, skipping initialization");
        return;
      }

      for (let role of roles) {
        await DB.Models.Role.create(role);
      }
    } catch (err) {
      console.log("ERROR INITIALIZE DATABASE ERROR", err.message);
      return;
    }
  }

  static async initializeClients() {
    try {
      let rawdata: any = fs.readFileSync(`${env.BASE_DIR}/dbInitialization/data/clients.json`);
      let clients = JSON.parse(rawdata);

      if ((await DB.Models.Client.find().countDocuments()) > 0) {
        console.log("Clients already contain values, skipping initialization");
        return;
      }

      for (let client of clients) await DB.Models.Client.create(client);
    } catch (err) {
      console.log("INITIALIZE CLIENTS ERROR", err.message);
      return;
    }
  }

  // static async initializeUsers() {
  //   try {
  //     let rawdata: any = fs.readFileSync(`${env.BASE_DIR}/dbInitialization/data/users.json`);
  //     let users = JSON.parse(rawdata);

  //     for (let user of users) {
  //       let u = await UserDetail.getByEmail(user.email);
  //       if (!u) {
  //         let returnedUser = await RegistrationMethods.register(user.email, user.password, user.user_type);
  //         await DB.Models.User.updateOne({ _id: returnedUser._id }, { isVerified: true });
  //       } else {
  //         console.log(`User ${user.email} already exist, skipping creation of user`);
  //       }
  //     }
  //   } catch (err) {
  //     console.log("INITIALIZE ADMIN ERROR", err.message);
  //     return;
  //   }
  // }
}
