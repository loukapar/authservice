import * as express from "express";
import { HttpStatusCode } from "../config/http";
import { SessionMethods } from "../services/session.service";
import { TokenMethods, RefreshTokenBody } from "../services/token.service";
import { JWT } from "../utils/global/jwt";
import Response from "../utils/response";

export default class TokenController {
  /**
   * Refresh access token
   * @param req
   * @param res
   */
  static async refreshAccessToken(req: express.Request, res: express.Response) {
    try {
      let payload = req.body as RefreshTokenBody;
      let response = await TokenMethods.refreshAccessToken(payload.session_id, payload.refresh_token);
      return new Response(HttpStatusCode.OK, response).sendJsonResponse(res);
    } catch (err) {
      console.log("REFRESH ACCESS TOKEN ERROR", err.message);
      return new Response(
        err._statusCode ?? HttpStatusCode.BAD_REQUEST,
        null,
        err._errorCode ?? null,
        err.message
      ).sendJsonResponse(res);
    }
  }

  /**
   * Verify any token
   * @param req
   * @param res
   */
  static async verifyToken(req: express.Request, res: express.Response) {
    try {
      let payload: { token: string } = req.body;
      let isValid = await JWT.isTokenValid(payload.token);
      return new Response(HttpStatusCode.OK, { isValid: isValid }).sendJsonResponse(res);
    } catch (err) {
      console.log("VERIFY TOKEN ERROR", err.message);
      return new Response(
        err._statusCode ?? HttpStatusCode.BAD_REQUEST,
        null,
        err._errorCode ?? null,
        err.message
      ).sendJsonResponse(res);
    }
  }

  static async getSession(req: express.Request, res: express.Response) {
    try {
      let session_id: string = req.query.session_id as string;
      let session = await SessionMethods.verifySession(session_id);
      return new Response(HttpStatusCode.OK, session).sendJsonResponse(res);
    } catch (err) {
      console.log("GET SESSION ERROR", err.message);
      return new Response(
        err._statusCode ?? HttpStatusCode.BAD_REQUEST,
        null,
        err._errorCode ?? null,
        err.message
      ).sendJsonResponse(res);
    }
  }
}
