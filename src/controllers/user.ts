import express from "express";
import DB from "../utils/database";
import Response from "../utils/response";
import {
  RegistrationMethods,
  RegistrationBody,
  SocialRegistrationBody,
} from "../services/user.services/register.service";
import { HttpStatusCode } from "../config/http";
import { LoginMethods, LoginBody } from "../services/user.services/login.service";
import { UserDetail } from "../services/user.services/user.detail.service";
import ApiError from "../utils/error";
import ErrorCodes from "../config/error";
import { SessionMethods } from "../services/session.service";
import { IUserTypes } from "../config/user";
import env from "../config/environment";
import { verifySocialAuth } from "../utils/socialAuth";

export default class UserController {
  /**
   * Return all users registered in authentication service
   * @param req
   * @param res
   */
  static async getUsers(req: express.Request, res: express.Response) {
    let response = await DB.Models.User.find({});
    return new Response(200, response).sendJsonResponse(res);
  }

  /**
   * Register user in authentication service
   * @param req
   * @param res
   */
  static async register(req: express.Request, res: express.Response) {
    try {
      let userType: string = req.params.userType;
      let payload = req.body as RegistrationBody;
      let user = await RegistrationMethods.register(payload.email.toLowerCase(), userType, payload.password);
      return new Response(HttpStatusCode.OK, user).sendJsonResponse(res);
    } catch (err) {
      console.log("REGISTER ERROR", err.message);
      return new Response(
        err._statusCode ?? HttpStatusCode.BAD_REQUEST,
        null,
        err._errorCode ?? null,
        err.message
      ).sendJsonResponse(res);
    }
  }

  /**
   * Login user
   * @param req
   * @param res
   */
  static async login(req: express.Request, res: express.Response) {
    try {
      let clientName: string = req.headers.from as string;
      let payload = req.body as LoginBody;

      // check if user is regular user
      const userData = await UserDetail.getByEmail(payload.email);
      if (userData?.socialAuth?.authid && !userData.password) {
        throw new ApiError("Invalid Credentials", ErrorCodes.INVALID_USER, HttpStatusCode.BAD_REQUEST);
      }

      let response = await LoginMethods.login(payload.email.toLowerCase(), payload.password, clientName);
      return new Response(HttpStatusCode.OK, response).sendJsonResponse(res);
    } catch (err) {
      console.log("LOGIN ERROR", err.message);
      return new Response(
        err._statusCode ?? HttpStatusCode.BAD_REQUEST,
        null,
        err._errorCode ?? null,
        err.message
      ).sendJsonResponse(res);
    }
  }

  /**
   * Social Login
   * @param req
   * @param res
   * @returns
   */
  static async socialLogin(req: express.Request, res: express.Response) {
    try {
      let clientName: string = req.headers.from as string;
      let userType: string = req.params.userType;
      // console.log("Type::!!", userType);
      let payload = req.body as SocialRegistrationBody;
      await verifySocialAuth(payload.token, payload.provider as any);
      await RegistrationMethods.socialRegisterOrUpdate(userType, payload);
      let response = await LoginMethods.login(payload.email.toLowerCase(), null, clientName);

      return new Response(HttpStatusCode.OK, response).sendJsonResponse(res);
    } catch (err) {
      console.log("LOGIN ERROR", err.message);
      return new Response(
        err._statusCode ?? HttpStatusCode.BAD_REQUEST,
        null,
        err._errorCode ?? null,
        err.message
      ).sendJsonResponse(res);
    }
  }

  /**
   * Delete user from authentication service
   * @param req
   * @param res
   */
  static async deleteUser(req: express.Request, res: express.Response) {
    try {
      let userID: string = req.params.id;
      await UserDetail.delete(userID);
      return new Response(HttpStatusCode.OK, { _id: userID }).sendJsonResponse(res);
    } catch (err) {
      console.log("DELETE USER ERROR", err.message);
      return new Response(
        err._statusCode ?? HttpStatusCode.BAD_REQUEST,
        null,
        err._errorCode ?? null,
        err.message
      ).sendJsonResponse(res);
    }
  }

  /**
   * Change password of user
   * @param req
   * @param res
   */
  static async changePassword(req: express.Request, res: express.Response) {
    try {
      let userID: string = req.params.id;
      let currentPassword = req.body.current_password;
      let newPassword = req.body.new_password;
      await UserDetail.changePassword(userID, currentPassword, newPassword);
      return new Response(HttpStatusCode.OK, { _id: userID }).sendJsonResponse(res);
    } catch (err) {
      console.log("CHANGE PASSWORD ERROR", err.message);
      return new Response(
        err._statusCode ?? HttpStatusCode.BAD_REQUEST,
        null,
        err._errorCode ?? null,
        err.message
      ).sendJsonResponse(res);
    }
  }

  /**
   * Verify user's email from unique link sent to email
   * @param req
   * @param res
   */
  static async verifyAccount(req: express.Request, res: express.Response) {
    try {
      let otvl: string = (req.query.token as string) ?? null;
      let email: string = (req.query.email as string).toLowerCase() ?? null;
      let client: string = (req.query.client as string).toUpperCase() ?? null;
      if (!otvl || !email)
        throw new ApiError(
          "Invalid verification link",
          ErrorCodes.INVALID_VERIFICATION_LINK,
          HttpStatusCode.BAD_REQUEST
        );
      await UserDetail.verifyAccount(otvl, email);

      let redirectClient = env.CLIENT;
      if (client.toUpperCase() === IUserTypes.PHARMACH) redirectClient = env.PHARMACY;
      else if (client.toUpperCase() === IUserTypes.ADMIN) redirectClient = env.ADMIN;

      res.writeHead(302, {
        // redirect to a page that says that account is now verified
        Location: `${redirectClient}/login?verified=true`,
      });
      res.end();
    } catch (err) {
      console.log("VERIFY ACCOUNT ERROR", err.message);
      return new Response(
        err._statusCode ?? HttpStatusCode.BAD_REQUEST,
        null,
        err._errorCode ?? null,
        err.message
      ).sendJsonResponse(res);
    }
  }

  /**
   * Verify that session_id exists and access token is valid. Returns session if it's verified.
   * @param req
   * @param res
   */
  static async verifySession(req: express.Request, res: express.Response) {
    try {
      let session_id: string = (req.body as { session_id: string }).session_id;
      let session = await SessionMethods.verifySession(session_id);
      return new Response(HttpStatusCode.OK, session).sendJsonResponse(res);
    } catch (err) {
      console.log("VERIFY SESSION ERROR", err.message);
      return new Response(
        err._statusCode ?? HttpStatusCode.BAD_REQUEST,
        null,
        err._errorCode ?? null,
        err.message
      ).sendJsonResponse(res);
    }
  }

  /**
   * Logout user
   * @param req
   * @param res
   */
  static async logOut(req: express.Request, res: express.Response) {
    try {
      let session_id = req.body.session_id ?? null;
      let access_token = req.body.access_token ?? null;
      if (!session_id && !access_token)
        throw new ApiError(
          "Invalid logout input. Provide either session_id or access_token",
          null,
          HttpStatusCode.BAD_REQUEST
        );
      // let user_id = await SessionMethods.getUserIdByTokenOrSession(session_id, access_token);
      // if (user_id) await SessionMethods.logOutUser(user_id);
      return new Response(HttpStatusCode.OK, null).sendJsonResponse(res);
    } catch (err) {
      console.log("LOGOUT ERROR", err.message);
      return new Response(
        err._statusCode ?? HttpStatusCode.BAD_REQUEST,
        null,
        err._errorCode ?? null,
        err.message
      ).sendJsonResponse(res);
    }
  }

  /**
   * Send email to user with reset password link
   * @param req
   * @param res
   */
  static async forgotPassword(req: express.Request, res: express.Response) {
    try {
      let email = (req.body.email as string).toLowerCase() ?? null;
      let from = (req.body.from as string).toUpperCase() ?? null;
      await UserDetail.forgotPassword(email, from);
      return new Response(HttpStatusCode.OK, null).sendJsonResponse(res);
    } catch (err) {
      console.log("FORGOT PASSWORD ERROR", err.message);
      return new Response(
        err._statusCode ?? HttpStatusCode.BAD_REQUEST,
        null,
        err._errorCode ?? null,
        err.message
      ).sendJsonResponse(res);
    }
  }

  /**
   * Reset user password
   * @param req
   * @param res
   */
  static async resetPassword(req: express.Request, res: express.Response) {
    try {
      let authorizationHeader = req.headers.authorization;
      if (!authorizationHeader)
        throw new ApiError(
          "Reset password token is missing",
          ErrorCodes.RESET_PASSWORD_INVALID_TOKEN,
          HttpStatusCode.BAD_REQUEST
        );
      let token = authorizationHeader.split(" ")[1];

      let { password, confirm_password } = req.body;
      await UserDetail.resetPassword(token, password, confirm_password);
      return new Response(HttpStatusCode.OK, null).sendJsonResponse(res);
    } catch (err) {
      console.log("RESET PASSWORD ERROR", err.message);
      return new Response(
        err._statusCode ?? HttpStatusCode.BAD_REQUEST,
        null,
        err._errorCode ?? null,
        err.message
      ).sendJsonResponse(res);
    }
  }

  static async blockUser(req: express.Request, res: express.Response) {
    try {
      let user_id = req.body.user_id;
      if (!user_id) throw new ApiError("Invalid user", ErrorCodes.INVALID_USER, HttpStatusCode.BAD_REQUEST);
      let session_id = await UserDetail.blockUser(user_id);
      return new Response(HttpStatusCode.OK, { session_id }).sendJsonResponse(res);
    } catch (err) {
      console.log("BLOCK USER ERROR", err.message);
      return new Response(
        err._statusCode ?? HttpStatusCode.BAD_REQUEST,
        null,
        err._errorCode ?? null,
        err.message
      ).sendJsonResponse(res);
    }
  }

  static async unblockUser(req: express.Request, res: express.Response) {
    try {
      let user_id = req.body.user_id;
      if (!user_id) throw new ApiError("Invalid user", ErrorCodes.INVALID_USER, HttpStatusCode.BAD_REQUEST);
      await UserDetail.unblockUser(user_id);
      return new Response(HttpStatusCode.OK, null).sendJsonResponse(res);
    } catch (err) {
      console.log("UNBLOCK USER ERROR", err.message);
      return new Response(
        err._statusCode ?? HttpStatusCode.BAD_REQUEST,
        null,
        err._errorCode ?? null,
        err.message
      ).sendJsonResponse(res);
    }
  }

  /**
   * Check if user exist
   * @param req
   * @param res
   */
  static async userExist(req: express.Request, res: express.Response) {
    let response = await DB.Models.User.findOne({ email: req.params.email });
    return new Response(200, { email: response?.email, _id: response?._id, exist: Boolean(response) }).sendJsonResponse(
      res
    );
  }
}
