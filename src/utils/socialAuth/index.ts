import axios from "axios";
import { HttpStatusCode } from "../../config/http";
import _ from "lodash";
import ApiError from "../error";
import ErrorCodes from "../../config/error";

const verifyGoogleAuth = async (token: string) => {
  try {
    await axios.post(`https://www.googleapis.com/oauth2/v1/tokeninfo?access_token=${token}`);
  } catch (error) {
    throw new ApiError(
      _.get(error, "message", "Invalid token"),
      ErrorCodes.ACCESS_TOKEN_NOT_VALID,
      HttpStatusCode.UNAUTHORIZED
    );
  }
};

const verifyFacebookAuth = async (token: string) => {
  try {
    await axios.get(`https://graph.facebook.com/me?access_token=${token}`);
  } catch (error) {
    throw new ApiError(
      _.get(error, "message", "Invalid token"),
      ErrorCodes.ACCESS_TOKEN_NOT_VALID,
      HttpStatusCode.UNAUTHORIZED
    );
  }
};

export const verifySocialAuth = async (token: string, provider: "google" | "facebook") => {
  switch (provider) {
    case "facebook":
      await verifyFacebookAuth(token);
      break;
    case "google":
      await verifyGoogleAuth(token);
      break;
    default:
      throw new Error("Wrong provider");
  }
};
