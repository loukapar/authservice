import { connect, connection, Connection, Error } from "mongoose";
import InitializeDatabase from "../../dbInitialization";
import { Client, ClientModel } from "../../models/client";
import { Otvl, OtvlModel } from "../../models/oneTimeVerificationLink";
import { RefreshTokenModel, RefreshToken } from "../../models/refreshToken";
import { Role, RoleModel } from "../../models/role";
import { Session, SessionModel } from "../../models/session";
import { User, UserModel } from "../../models/user";
import env from "../../config/environment";

declare interface IModels {
  User: UserModel;
  Role: RoleModel;
  Client: ClientModel;
  Session: SessionModel;
  RefreshToken: RefreshTokenModel;
  Otvl: OtvlModel;
}

export default class DB {
  private static instance: DB;

  private _db: Connection;
  private _models: IModels;

  private constructor() {
    let url = "";
    if (env.MONGO_DB_USERNAME && env.MONGO_DB_PASSWORD) {
      url = `mongodb://${env.MONGO_DB_USERNAME}:${env.MONGO_DB_PASSWORD}@${env.MONGO_DB_IP}:27017/${env.MONGO_DB_NAME}?authSource=admin`;
    } else {
      url = `mongodb://${env.MONGO_DB_IP}:27017/${env.MONGO_DB_NAME}`;
    }

    console.log("Db url: " + url);
    connect(
      // "mongodb+srv://pharmacy:1q2w3e4r5t@cluster0.xyv7x.mongodb.net/pharmacyoauth?retryWrites=true&w=majority",
      url,
      // `mongodb://${env.MONGO_DB_USERNAME}:${env.MONGO_DB_PASSWORD}@${env.MONGO_DB_IP}:27017/${env.MONGO_DB_NAME}?authSource=admin`,
      {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useCreateIndex: true,
      }
    );
    this._db = connection;
    this._db.on("open", this.connected);
    this._db.on("error", this.error);

    this._models = {
      User: new User().model,
      Role: new Role().model,
      Client: new Client().model,
      Session: new Session().model,
      RefreshToken: new RefreshToken().model,
      Otvl: new Otvl().model,
    };
  }

  public static get Models(): IModels {
    if (!DB.instance) {
      DB.instance = new DB();
    }
    return DB.instance._models;
  }

  private async connected() {
    console.log("Mongoose has connected, initializing database...");
    await InitializeDatabase.initializeRoles();
    await InitializeDatabase.initializeClients();
    console.log("Database initialization completed");
  }

  private error(error: Error): void {
    console.log("Mongoose has errored", error);
  }
}
