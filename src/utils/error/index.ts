import { HttpStatusCode } from "../../config/http";

export default class ApiError extends Error {
  readonly _statusCode;
  readonly _errorCode;

  constructor(
    message: string,
    errorcode: number | null = null,
    statusCode: number = HttpStatusCode.BAD_REQUEST
  ) {
    super(message);
    this._statusCode = statusCode;
    this._errorCode = errorcode;
  }
}
