import ErrorCodes from "../../config/error";
import ApiError from "../error";
import * as bcrypt from "bcrypt";
import env from "../../config/environment";
import RandExp from "randexp";

export default class Utils {
  private static emailRegex =
    /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  private static passwordRegex = /^(?=.*[a-zA-Z])(?=.*[0-9])[a-zA-Z0-9!@#$%^&*_]{8,32}/;

  public static isValidEmail(email: string): boolean {
    let emailValid = Utils.emailRegex.test(String(email).toLowerCase());
    if (!emailValid) throw new ApiError("Invalid email format", ErrorCodes.INVALID_EMAIL_FORMAT);
    return emailValid;
  }

  public static isValidPassword(password: string): boolean {
    let passwordValid = Utils.passwordRegex.test(String(password));
    if (!passwordValid) throw new ApiError("Invalid password format", ErrorCodes.INVALID_PASSWORD_FORMAT);
    return passwordValid;
  }

  public static generateRandomPassword(): string {
    return new RandExp(Utils.passwordRegex).gen();
  }

  public static async hash(plaintext: string) {
    const salt = await bcrypt.genSalt(env.SALT_ROUNDS);
    const hash = await bcrypt.hash(plaintext, salt);
    return { salt, hash };
  }

  public static async compareHash(plaintext: string, hash: string) {
    if (!plaintext || !hash) return false;

    let result = await bcrypt.compare(plaintext, hash);
    return result;
  }
}
