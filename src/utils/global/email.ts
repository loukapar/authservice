import { IUser } from "../../models/user";
import axios from "axios";
import env from "../../config/environment";

interface EmailBody {
  sender?: string;
  receivers: string[];
  subject: string;
  text: string;
  HTMLtemplate: string;
  attachments: Attachment[];
}

interface Attachment {
  filename: string;
  content: string;
  encoding: string;
}

export default class EmailService {
  public static async sendVerificationEmail(verificationLink: string, user: IUser) {
    let emailBody = `<h1>${env.NAME}</h1> <p>Hello, </p> <p>You are now an official member of the ${env.NAME} community. To get started, you need to verify your email address by clicking this <a href="${verificationLink}">link</a>.  This link will be active for 2 days. </p><p>Thanks for joining, ${env.NAME}</p>`;
    let body: EmailBody = {
      sender: env.EMAIL_SERVICE_SENDER,
      receivers: [user.email],
      subject: `Complete your ${env.NAME} registration`,
      text: "",
      HTMLtemplate: emailBody,
      attachments: [],
    };

    let result = await axios.post(`${env.EMAIL_SERVICE}/send`, body);
    // console.log("Result: email:", result);
  }

  public static async sendForgotPasswordEmail(link: string, user: IUser) {
    let emailBody = `<h1>${env.NAME}</h1> <p>Hello, </p> <p>It seems that you have asked to reset your password. You can change your password by clicking this <a href="${link}">link</a>.  This link will be active for 2 hours.  If you have never asked to reset your password please ignore this email. </p><p>Thank you, ${env.NAME}</p>`;
    let body: EmailBody = {
      sender: env.EMAIL_SERVICE_SENDER,
      receivers: [user.email],
      subject: `${env.NAME} - Forgot password`,
      text: "",
      HTMLtemplate: emailBody,
      attachments: [],
    };
    let result = await axios.post(`${env.EMAIL_SERVICE}/send`, body);
    // console.log("Result: email:", result);
  }
}
