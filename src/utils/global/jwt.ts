import * as jwt from "jsonwebtoken";
import * as fs from "fs";
import env from "../../config/environment";
import ApiError from "../error";
import ErrorCodes from "../../config/error";
import { HttpStatusCode } from "../../config/http";
import { UserRoles } from "../../config/user";

export interface AccessTokenPayload {
  role: UserRoles[];
  email: string;
}

export class JWT {
  public static async issueJWT(payload: AccessTokenPayload | {}, subject: string, expiresIn: string): Promise<string> {
    let privateKey = fs.readFileSync("private.key", "utf-8");

    let signOptions: jwt.SignOptions = {
      issuer: env.TOKEN_ISSUER,
      subject: subject,
      audience: env.TOKEN_AUDIENCE,
      algorithm: "RS256",
      expiresIn: expiresIn,
    };

    let token = await jwt.sign(payload, privateKey, signOptions);
    return token;
  }

  public static async verifyToken(token: string, tokenType: "ACCESS" | "REFRESH") {
    try {
      let publicKey = fs.readFileSync("public.key", "utf-8");
      await jwt.verify(token, publicKey, {
        ignoreExpiration: false,
        audience: env.TOKEN_AUDIENCE,
        issuer: env.TOKEN_ISSUER,
      });
    } catch (err) {
      // console.log("VERIFY TOKEN ERROR", err.message);
      throw new ApiError(
        err.message,
        tokenType === "ACCESS" ? ErrorCodes.ACCESS_TOKEN_NOT_VALID : ErrorCodes.REFRESH_TOKEN_NOT_VALID,
        HttpStatusCode.UNAUTHORIZED
      );
    }
  }

  public static async isTokenValid(token: string): Promise<boolean> {
    try {
      let publicKey = fs.readFileSync("public.key", "utf-8");
      await jwt.verify(token, publicKey, {
        ignoreExpiration: false,
        audience: env.TOKEN_AUDIENCE,
        issuer: env.TOKEN_ISSUER,
      });
      return true;
    } catch (err) {
      return false;
    }
  }

  public static decode(token: string) {
    try {
      let decoded = jwt.decode(token, { complete: true });
      if (!decoded) return {};
      return decoded;
    } catch (err) {
      throw new ApiError(err.message, null, HttpStatusCode.UNAUTHORIZED);
    }
  }
}
