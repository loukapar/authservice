import { Schema, model, Document, Model } from "mongoose";
import { AllUserRoles, UserRoles } from "../config/user";

export declare interface IRole extends Document {
  name: string;
  role_id: UserRoles;
}

export interface RoleModel extends Model<IRole> {}

export class Role {
  private _model: Model<IRole>;

  constructor() {
    const schema = new Schema({
      name: { type: String, required: true, unique: true },
      role_id: { type: String, required: true, unique: true, enum: AllUserRoles },
    });

    this._model = model<IRole>("Role", schema);
  }

  public get model(): Model<IRole> {
    return this._model;
  }
}
