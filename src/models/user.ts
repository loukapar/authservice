import { Schema, model, Document, Model } from "mongoose";
import { AllUserRoles, UserRoles } from "../config/user";

export declare interface IUser extends Document {
  email: string;
  password: string | null;
  salt: string | null;
  roles: UserRoles[];
  isVerified?: boolean;
  isBlocked?: boolean;
  socialAuth?: ISocialAuth;
}

export interface ISocialAuth {
  token?: string;
  refreshToken?: string;
  provider?: string;
  authid: string;
}

export interface UserModel extends Model<IUser> {}

export class User {
  private _model: Model<IUser>;

  constructor() {
    const schema = new Schema({
      email: { type: String, required: true, unique: true },
      password: { type: String, default: null },
      salt: { type: String, default: null },
      roles: [{ type: String, required: true, enum: AllUserRoles }],
      isVerified: { type: Boolean, default: false },
      isBlocked: { type: Boolean, default: false },
      socialAuth: {
        type: new Schema({
          token: { type: String },
          refreshToken: { type: String },
          provider: { type: String },
          authid: { type: String, unique: true, sparse: true },
        }),
        required: false,
      },
    });

    this._model = model<IUser>("User", schema);
  }

  public get model(): Model<IUser> {
    return this._model;
  }
}
