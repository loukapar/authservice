import { Schema, model, Document, Model } from "mongoose";

export declare interface ISession extends Document {
  session_id: string;
  access_token: string;
  user_id: string;
}

export interface SessionModel extends Model<ISession> {}

export class Session {
  private _model: Model<ISession>;

  constructor() {
    const schema = new Schema({
      session_id: { type: String, required: true, unique: true },
      access_token: { type: String, required: true, unique: true },
      user_id: { type: String, required: true, unique: true },
    });

    this._model = model<ISession>("Session", schema);
  }

  public get model(): Model<ISession> {
    return this._model;
  }
}
