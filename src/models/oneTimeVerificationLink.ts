import { Schema, model, Document, Model } from "mongoose";

export declare interface IOTVL extends Document {
  link: string;
  user_id: string;
}

export interface OtvlModel extends Model<IOTVL> {}

export class Otvl {
  private _model: Model<IOTVL>;

  constructor() {
    const schema = new Schema({
      link: { type: String, required: true, unique: true },
      user_id: { type: String, required: true, unique: true },
    });

    this._model = model<IOTVL>("OTVL", schema);
  }

  public get model(): Model<IOTVL> {
    return this._model;
  }
}
