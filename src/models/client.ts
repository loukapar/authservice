import { Schema, model, Document, Model } from "mongoose";
import { AllUserRoles, UserRoles } from "../config/user";

export declare interface IClient extends Document {
  name: string;
  allowedRoles: Array<UserRoles>;
}

export interface ClientModel extends Model<IClient> {}

export class Client {
  private _model: Model<IClient>;

  constructor() {
    const schema = new Schema({
      name: { type: String, required: true, unique: true },
      allowedRoles: [{ type: String, enum: AllUserRoles }],
    });

    this._model = model<IClient>("Client", schema);
  }

  public get model(): Model<IClient> {
    return this._model;
  }
}
