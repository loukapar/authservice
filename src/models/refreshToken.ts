import { Schema, model, Document, Model } from "mongoose";

export declare interface IRefreshToken extends Document {
  refresh_token: string;
  user_id: string;
}

export interface RefreshTokenModel extends Model<IRefreshToken> {}

export class RefreshToken {
  private _model: Model<IRefreshToken>;

  constructor() {
    const schema = new Schema({
      refresh_token: { type: String, required: true, unique: true },
      user_id: { type: String, required: true, unique: true },
    });

    this._model = model<IRefreshToken>("RefreshToken", schema);
  }

  public get model(): Model<IRefreshToken> {
    return this._model;
  }
}
