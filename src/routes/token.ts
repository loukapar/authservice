import express from "express";
import TokenController from "../controllers/token";
import TokenValidators from "../validators/token.validator";
/**
 * App Variables
 */
const router = express.Router();

router.route("/refresh").post(TokenValidators.refreshAccessTokenValidator, TokenController.refreshAccessToken);
router.route("/verify").post(TokenValidators.verifyTokenValidator, TokenController.verifyToken);
router.route("/session").get(TokenValidators.getSessionValidator, TokenController.getSession);

export default router;
