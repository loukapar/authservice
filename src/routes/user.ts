import express from "express";
import UserController from "../controllers/user";
import UserValidators from "../validators/user.validator";
/**
 * App Variables
 */
const router = express.Router();

router.route("").get(UserController.getUsers);
router
  .route("/:id")
  .delete(UserController.deleteUser)
  .patch(UserValidators.updateUserPasswordValidator, UserController.changePassword);
router.route("/register/:userType").post(UserValidators.registerAndLoginValidator, UserController.register);
router.route("/login").post(UserValidators.registerAndLoginValidator, UserController.login);
router.route("/social-login/:userType").post(UserValidators.socialLoginValidator, UserController.socialLogin);
router.route("/verify/account").get(UserValidators.verifyAccountValidator, UserController.verifyAccount);
router.route("/block").post(UserValidators.blockUserValidator, UserController.blockUser);
router.route("/unblock").post(UserValidators.blockUserValidator, UserController.unblockUser);
router.route("/session/verify").post(UserValidators.verifySessionValidator, UserController.verifySession);
router.route("/logout").post(UserValidators.logoutValidator, UserController.logOut);
router.route("/forgot-password").post(UserValidators.forgotPasswordValidator, UserController.forgotPassword);
router.route("/reset-password").post(UserValidators.resetPasswordValidator, UserController.resetPassword);
router.route("/exist/:email").get(UserController.userExist);

export default router;
