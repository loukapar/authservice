import { HttpStatusCode } from "../config/http";
import DB from "../utils/database";
import ApiError from "../utils/error";

export default class ClientMethods {
  /**
   * Return list of clients
   */
  public static async getClients() {
    let clients = await DB.Models.Client.find({});
    return clients;
  }

  /**
   * Check if client is valid by name, returns client
   * @param clientName Client name
   */
  public static async isValidClient(clientName: string | null) {
    if (!clientName)
      throw new ApiError(`Client name is not valid`, null /*ErrorCodes.INVALID_CLIENT*/, HttpStatusCode.BAD_REQUEST);

    let client = await DB.Models.Client.findOne({
      name: clientName.toUpperCase(),
    });
    if (!client)
      throw new ApiError(
        `Client ${clientName} is not valid`,
        null /*ErrorCodes.INVALID_CLIENT*/,
        HttpStatusCode.BAD_REQUEST
      );

    return client;
  }
}
