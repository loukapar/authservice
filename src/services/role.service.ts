import { HttpStatusCode } from "../config/http";
import { UserRoles } from "../config/user";
import DB from "../utils/database";
import ApiError from "../utils/error";

export default class RoleMethods {
  /**
   * return roles
   */
  public static async getRoles() {
    let roles = await DB.Models.Role.find({});
    return roles;
  }

  /**
   * Get role id by role name
   * @param roleName Role name
   */
  public static async getRoleId(roleName: string) {
    let role = await DB.Models.Role.findOne({ name: roleName.toUpperCase() });

    if (!role)
      throw new ApiError(`Role ${roleName} is not valid`, null /*ErrorCodes.INVALID_ROLE*/, HttpStatusCode.BAD_REQUEST);
    return role.role_id;
  }

  /**
   * Get role name by id
   * @param roleId Role id
   */
  public static async getRoleName(roleId: UserRoles) {
    let role = await DB.Models.Role.findOne({ role_id: roleId });

    if (!role)
      throw new ApiError(
        `Role with id ${roleId} is not valid`,
        null /*ErrorCodes.INVALID_ROLE*/,
        HttpStatusCode.BAD_REQUEST
      );
    return role.name;
  }
}
