import ErrorCodes from "../../config/error";
import { HttpStatusCode } from "../../config/http";
import { UserRoles, UserType, UserTypeRoles } from "../../config/user";
import { Role } from "../../models/role";
import DB from "../../utils/database";
import ApiError from "../../utils/error";
import Utils from "../../utils/global";
import EmailService from "../../utils/global/email";
import { OtvlMethods } from "../otvl.service";

export interface RegistrationBody {
  email: string;
  password: string;
}

export interface SocialRegistrationBody {
  email: string;
  token: string;
  refreshToken: string;
  provider: string;
  authid: string;
}

interface IUserCreate {
  email: string;
  password?: string;
  salt?: string;
  roles: UserRoles[];
  isVerified?: boolean;
  social?: Omit<SocialRegistrationBody, "email">;
}

export class RegistrationMethods {
  /**
   * Implements the registration of a user
   * @param email Email of user
   * @param password Password of user
   * @param userType Type of user
   */
  public static async register(email: string, userType: string, password: string) {
    let roles = RegistrationMethods.isUserTypeValid(userType);
    RegistrationMethods.isUserInfoValid(email, password);
    await RegistrationMethods.isEmailAlreadyInUse(email);
    let hashResult = await Utils.hash(password);
    let user = await RegistrationMethods.createUser({
      email,
      password: hashResult.hash,
      roles,
      salt: hashResult.salt,
    });
    // verify email
    let otvlToken = await OtvlMethods.createOTVL(user._id, email);
    await EmailService.sendVerificationEmail(OtvlMethods.constructOTVLUrl(user.email, otvlToken, userType), user);
    let returnUser = {
      roles: user.roles,
      _id: user._id,
      email: user.email,
    };
    return returnUser;
  }

  /**
   * Check if user's given type is correct. Possible values CLIENT OR PHARMACY
   * @param userType User type from url parameter
   */
  private static isUserTypeValid(userType: string) {
    if (!UserType.includes(userType.toUpperCase() as any))
      throw new ApiError("Path does not exist", HttpStatusCode.NOT_FOUND);
    return UserTypeRoles[UserType.indexOf(userType.toUpperCase() as any)];
  }

  /**
   * Check if user's input is valid based on regex
   * @param email Email of user
   * @param password Password of user
   */
  private static isUserInfoValid(email: string, password?: string): boolean {
    let valid = Utils.isValidEmail(email);
    if (password) {
      valid = valid && Utils.isValidPassword(password);
    }
    return valid;
  }

  /**
   * Check if given email is not already in use
   * @param email Email of user
   */
  private static async isEmailAlreadyInUse(email: string) {
    let user = await DB.Models.User.findOne({ email: email });
    if (user) {
      throw new ApiError(`User with email ${email} already exists`, ErrorCodes.EMAIL_EXIST);
    }
  }

  // public static async userExist(email: string) {
  //   let user = await DB.Models.User.findOne({ email: email });
  //   if (user) {
  //     return user;
  //   }

  //   return false;
  // }

  /**
   * Create and post user in db
   * @param email Email of user
   * @param password Password of user
   * @param role Role of user
   */

  // IUserCreate
  private static async createUser({ email, password, salt, roles, social, isVerified }: IUserCreate) {
    //     email: string, password: string, role: UserRoles[], salt: string

    let newUser = await DB.Models.User.create({
      email: email,
      password: password || null,
      salt: salt || null,
      roles,
      socialAuth: social,
      isVerified,
    });
    return newUser;
  }

  /**
   *
   * Social Register
   * @param email
   * @param password
   * @param userType
   * @returns
   */
  public static async socialRegisterOrUpdate(
    userType: string,
    { email, provider, token, refreshToken, authid }: SocialRegistrationBody
  ) {
    const _user = await DB.Models.User.findOne({ email: email });
    if (_user) {
      if (
        _user.socialAuth?.authid === authid &&
        _user.socialAuth.provider === provider &&
        _user.socialAuth.authid === authid
      ) {
        // update user only
        _user.socialAuth = {
          provider,
          token,
          refreshToken,
          authid,
        };
        await _user.save();
        return;
      } else {
        throw new ApiError("Wrong Credentials", ErrorCodes.INVALID_USER);
      }
    }

    let roles = RegistrationMethods.isUserTypeValid(userType);
    RegistrationMethods.isUserInfoValid(email);
    await RegistrationMethods.isEmailAlreadyInUse(email);
    // let hashResult = await Utils.hash(password);
    await RegistrationMethods.createUser({
      email,
      roles,
      isVerified: true,
      social: {
        token,
        refreshToken,
        provider,
        authid,
      },
    });
    // // verify email
    // let otvlToken = await OtvlMethods.createOTVL(user._id, email);
    // await EmailService.sendVerificationEmail(OtvlMethods.constructOTVLUrl(user.email, otvlToken, userType), user);
    // let returnUser = {
    //   roles: user.roles,
    //   _id: user._id,
    //   email: user.email,
    // };
    // return returnUser;
  }
}
