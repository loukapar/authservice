import ErrorCodes from "../../config/error";
import { HttpStatusCode } from "../../config/http";
import { IUser } from "../../models/user";
import DB from "../../utils/database";
import ApiError from "../../utils/error";
import Utils from "../../utils/global";
import { JWT } from "../../utils/global/jwt";
import ClientMethods from "../client.service";
import { TokenMethods } from "../token.service";
import env from "../../config/environment";
import { SessionMethods } from "../session.service";
import { ISession } from "../../models/session";
import { UserDetail } from "./user.detail.service";
import { UserRoles } from "../../config/user";

export interface LoginBody {
  email: string;
  password: string;
}

interface LoginResponse {
  access_token: string;
  refresh_token: string;
  session_id: string;
  user_id: string;
}

export class LoginMethods {
  /**
   * Login user to client
   * @param email Email of user
   * @param password Password of user
   * @param clientName Client to login to
   */
  public static async login(email: string, password: string | null, clientName: string): Promise<LoginResponse> {
    let client = await ClientMethods.isValidClient(clientName);
    let user = await LoginMethods.validateUserEmail(email);
    if (password) {
      await LoginMethods.validateUserPassword(password, user.password || "");
    }
    LoginMethods.accessPermission(user.roles, client.allowedRoles, user.email, client.name);
    LoginMethods.userBlocked(user);
    LoginMethods.userAccoundVerified(user);
    // check if user has a refresh token stored
    let refreshToken: string | null = await TokenMethods.checkIfUserHasActiveRefreshToken(user._id);
    // if user has a refresh token in the DB
    if (refreshToken) {
      // if user has a refresh token check if it's valid
      if (await JWT.isTokenValid(refreshToken)) {
        // if it's valid then find session of user
        let session: ISession | null = await SessionMethods.getSessionByUserID(user._id);
        // in case session exist in db
        if (session) {
          // in case session of user exists then check if the access_token is valid
          if (await JWT.isTokenValid(session.access_token)) {
            // if token is valid then return the same session info
            return {
              access_token: session.access_token,
              session_id: session.session_id,
              refresh_token: refreshToken,
              user_id: user._id,
            };
          }
        }
      }
    }
    await SessionMethods.logOutUser(user._id);
    let accessToken = await JWT.issueJWT(
      await UserDetail.calculateAccessTokenPayload(user),
      user.email,
      env.ACCESS_TOKEN_EXPIRATION
    );
    let refreshTok = await JWT.issueJWT({ email: user.email }, user.email, env.REFRESH_TOKEN_EXPIRATION);
    await TokenMethods.storeRefreshToken(refreshTok, user._id);
    let sessionId = await SessionMethods.storeSession(user._id, accessToken);
    return {
      access_token: accessToken,
      refresh_token: refreshTok,
      session_id: sessionId,
      user_id: user._id,
    };
  }

  /**
   * Validates that the given email exists in db
   * @param email Email of user
   */
  public static async validateUserEmail(email: string) {
    let userExist = await DB.Models.User.findOne({ email: email });
    if (!userExist) {
      throw new ApiError(`Invalid email`, ErrorCodes.INVALID_USER);
    }
    return userExist;
  }

  /**
   * Compare plaintext password and hash password to be the same
   * @param password Password of user
   * @param hashPassword Hashed password
   */
  public static async validateUserPassword(password: string, hashPassword: string) {
    if (!(await Utils.compareHash(password, hashPassword))) {
      throw new ApiError("Invalid password", ErrorCodes.INVALID_USER);
    }
  }

  /**
   * Check if the user has access to the client that is trying to login based on the allowed roles of the client
   * @param userRoles Role of user
   * @param clientAllowedRoles Roles that allowed for a specific client
   * @param userEmail User email
   * @param clientName Client name
   */
  public static accessPermission(
    userRoles: UserRoles[],
    clientAllowedRoles: UserRoles[],
    userEmail: string,
    clientName: string
  ) {
    for (let userRole of userRoles) {
      if (clientAllowedRoles.includes(userRole)) {
        return;
      }
    }
    throw new ApiError(
      `User ${userEmail} has no permission to client ${clientName}`,
      ErrorCodes.ACCESS_DENIED,
      HttpStatusCode.FORBIDDEN
    );
  }

  /**
   * Check if the account of the user is verifies. If not verified then user is not allower to login
   * @param user User object
   */
  public static userAccoundVerified(user: IUser) {
    if (!user.isVerified)
      throw new ApiError(
        `Email ${user.email} is not verified`,
        ErrorCodes.USER_EMAIL_NOT_VERIFIED,
        HttpStatusCode.BAD_REQUEST
      );
  }

  public static userBlocked(user: IUser) {
    if (user.isBlocked)
      throw new ApiError(`Account has been deactivated`, ErrorCodes.ACCOUNT_BLOCKED, HttpStatusCode.BAD_REQUEST);
  }
}
