import { ClientEnum } from "../../config/client";
import env from "../../config/environment";
import ErrorCodes from "../../config/error";
import { HttpStatusCode } from "../../config/http";
import { UserRoles } from "../../config/user";
import { IUser } from "../../models/user";
import DB from "../../utils/database";
import ApiError from "../../utils/error";
import Utils from "../../utils/global";
import EmailService from "../../utils/global/email";
import { AccessTokenPayload, JWT } from "../../utils/global/jwt";
import ClientMethods from "../client.service";
import { OtvlMethods } from "../otvl.service";
import { SessionMethods } from "../session.service";

export class UserDetail {
  /**
   * Get user by id
   * @param id User id
   */
  static async getById(id: string): Promise<IUser> {
    let user = await DB.Models.User.findOne({ _id: id });
    if (!user) {
      throw new ApiError(`User with id ${id} does not exist`, ErrorCodes.USER_NOT_EXIST, HttpStatusCode.BAD_REQUEST);
    }
    return user;
  }

  /**
   * Get user by email
   * @param email
   */
  static async getByEmail(email: string): Promise<IUser | null> {
    let user = await DB.Models.User.findOne({ email: email });
    if (!user) return null;
    return user;
  }

  /**
   * Delete user by id
   * @param id User id
   */
  static async delete(id: string) {
    let response = await DB.Models.User.deleteOne({ _id: id });
    if (response.deletedCount == 0) {
      throw new ApiError(`User with id ${id} does not exist`, ErrorCodes.USER_NOT_EXIST, HttpStatusCode.BAD_REQUEST);
    }
  }

  /**
   * Change user's password
   * @param id User id
   * @param password User's current password
   * @param newPassword User's new password
   */
  static async changePassword(id: string, password: string, newPassword: string) {
    let user = await UserDetail.getById(id);
    if (user.socialAuth?.authid) {
      throw new ApiError(
        "User can not change password",
        ErrorCodes.CAN_NOT_CHANGE_PASSWORD,
        HttpStatusCode.BAD_REQUEST
      );
    }

    if (!user.password) {
      throw new ApiError(
        "User can not change his password",
        ErrorCodes.CAN_NOT_CHANGE_PASSWORD,
        HttpStatusCode.BAD_REQUEST
      );
    }
    // Check if currentPassword match with the password stored in DB
    if (!(await Utils.compareHash(password, user.password))) {
      throw new ApiError(
        `Given current password does not match with the stored one`,
        ErrorCodes.UPDATE_PASSWORD_NOT_MATCH,
        HttpStatusCode.BAD_REQUEST
      );
    }
    Utils.isValidPassword(newPassword);
    const hashResult = await Utils.hash(newPassword);
    await DB.Models.User.updateOne({ _id: id }, { password: hashResult.hash, salt: hashResult.salt });
  }

  /**
   * Calculate and returns the payload of user's access_token
   * @param user User object
   */
  public static async calculateAccessTokenPayload(user: IUser): Promise<AccessTokenPayload> {
    let payload = {
      role: user.roles,
      email: user.email,
    };
    return payload;
  }

  /**
   * Verifies user account by ensuring that the email is correct. This endpoint it's executed when the one time validation link sent to user's email is used.
   * @param otvl One time verification link
   * @param email User's email
   */
  public static async verifyAccount(otvl: string, email: string) {
    let otvlInstance = await DB.Models.Otvl.findOne({ link: otvl });
    if (!otvlInstance)
      throw new ApiError("Invalid verification link", ErrorCodes.INVALID_VERIFICATION_LINK, HttpStatusCode.NOT_FOUND);
    if (!JWT.isTokenValid(otvl))
      throw new ApiError("Verification link expired", ErrorCodes.VERIFICATION_LINK_EXPIRED, HttpStatusCode.NOT_FOUND);
    let user = await UserDetail.getById(otvlInstance.user_id);
    if (user.email != email)
      throw new ApiError(
        "Invalid email in verification link",
        ErrorCodes.INVALID_VERIFICATION_LINK,
        HttpStatusCode.BAD_REQUEST
      );
    await UserDetail.verifyUser(user._id);
    await OtvlMethods.deleteOTVL(otvlInstance._id);
  }

  static async forgotPassword(email: string, from: string | null) {
    let client = await ClientMethods.isValidClient(from);
    let user = await UserDetail.getByEmail(email);
    if (!user || user.socialAuth?.authid) return;
    let token = await JWT.issueJWT({ email: user.email }, user.email, "2h");
    let redirectUrl = env.CLIENT;
    if (client.name === ClientEnum.PHARMACY_WEB_APP) redirectUrl = env.PHARMACY;
    if (client.name === ClientEnum.ADMIN_WEB_APP) redirectUrl = env.ADMIN;
    let link = `${redirectUrl}/reset-password?token=${token}`;
    await EmailService.sendForgotPasswordEmail(link, user);
  }

  static async resetPassword(token: string, password: string, confirmPassword: string) {
    if (!JWT.isTokenValid(token))
      throw new ApiError(
        "Invalid token in reset password",
        ErrorCodes.RESET_PASSWORD_INVALID_TOKEN,
        HttpStatusCode.BAD_REQUEST
      );

    let decoded = JWT.decode(token);
    let email = (decoded as any).payload.email;
    let user = await UserDetail.getByEmail(email);
    if (!user)
      throw new ApiError(
        "Invalid email in resetting password flow",
        ErrorCodes.USER_NOT_EXIST,
        HttpStatusCode.BAD_REQUEST
      );

    if (user.socialAuth?.authid)
      throw new ApiError("Invalid action", ErrorCodes.CAN_NOT_CHANGE_PASSWORD, HttpStatusCode.BAD_REQUEST);

    if (password != confirmPassword)
      throw new ApiError(
        "Confirm password have to much with password",
        ErrorCodes.UPDATE_PASSWORD_NOT_MATCH,
        HttpStatusCode.BAD_REQUEST
      );
    Utils.isValidPassword(password);
    const hashResult = await Utils.hash(password);
    await DB.Models.User.updateOne({ _id: user.id }, { password: hashResult.hash, salt: hashResult.salt });
  }

  /**
   * Update user's isVerified field to true
   * @param user_id User id
   */
  private static async verifyUser(user_id: string) {
    await DB.Models.User.updateOne({ _id: user_id }, { isVerified: true });
  }

  static async blockUser(user_id: string) {
    let user = await UserDetail.getById(user_id);
    let session = await SessionMethods.getSessionByUserID(user_id);
    await DB.Models.User.updateOne({ _id: user.id }, { isBlocked: true });
    await SessionMethods.logOutUser(user.id);
    return session ? session.session_id : null;
  }

  static async unblockUser(user_id: string) {
    let user = await UserDetail.getById(user_id);
    await DB.Models.User.updateOne({ _id: user.id }, { isBlocked: false });
  }
}
