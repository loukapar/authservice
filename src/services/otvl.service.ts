import env from "../config/environment";
import DB from "../utils/database";
import { JWT } from "../utils/global/jwt";

export class OtvlMethods {
  /**
   * Creates a one time validation link and stores it in database. This link will be used to verify user's account/email
   * @param user_id User's id
   * @param email User's email
   */
  public static async createOTVL(user_id: string, email: string): Promise<string> {
    let otvl = await JWT.issueJWT({}, email, env.VERIFICATION_LINK_TOKEN_EXPIRATION);
    let otvlToken = await DB.Models.Otvl.create({
      link: otvl,
      user_id: user_id,
    });
    return otvlToken.link;
  }

  /**
   * Constructs the endpoint (url) that user will call in order to verify account/email
   * @param email User's email
   * @param otvl One time validation link
   */
  public static constructOTVLUrl(email: string, otvl: string, userType: string): string {
    return `${env.GATEWAY}/oauth/user/verify/account/?token=${otvl}&email=${email}&client=${userType.toLowerCase()}`;
  }

  /**
   * Deletes otvl after it used
   * @param id id of otvl
   */
  public static async deleteOTVL(id: string) {
    await DB.Models.Otvl.deleteOne({ _id: id });
  }
}
