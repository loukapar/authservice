import DB from "../utils/database";
import { v4 as uuidv4 } from "uuid";
import Utils from "../utils/global";
import { ISession } from "../models/session";
import { TokenMethods } from "./token.service";
import ApiError from "../utils/error";
import { HttpStatusCode } from "../config/http";
import { JWT } from "../utils/global/jwt";
import { IRefreshToken } from "../models/refreshToken";

export class SessionMethods {
  /**
   * Stores session { session_id, user_id, access_token } in database, returns session_id
   * @param userID User id
   * @param accessToken Access token
   */
  public static async storeSession(userID: string, accessToken: string): Promise<string> {
    let hashResult = await Utils.hash(uuidv4());
    await DB.Models.Session.create({
      session_id: hashResult.hash,
      user_id: userID,
      access_token: accessToken,
    });
    return hashResult.hash;
  }

  /**
   * Delete user's session by user id
   * @param userID User id
   */
  public static async deleteSession(userID: string) {
    await DB.Models.Session.deleteOne({ user_id: userID });
  }

  /**
   * Returns user's session by id or null
   * @param userID User id
   */
  public static async getSessionByUserID(userID: string) {
    let session: ISession | null = await DB.Models.Session.findOne({ user_id: userID });
    if (!session) {
      return null;
    }
    return session;
  }

  /**
   * Returns user's session by session id or null
   * @param sessionID
   */
  public static async getSessionBySessionId(sessionID: string) {
    let session: ISession | null = await DB.Models.Session.findOne({ session_id: sessionID });
    if (!session) return null;
    return session;
  }

  /**
   * logs out user by deleting session and refresh token
   * @param userID User id
   */
  public static async logOutUser(userID: string) {
    await SessionMethods.deleteSession(userID);
    await TokenMethods.deleteRefreshToken(userID);
  }

  /**
   * Verify that user's session is valid and access token is not expired, returns session
   * @param session_id Session id
   */
  public static async verifySession(session_id: string) {
    let session: ISession | null = await DB.Models.Session.findOne({ session_id: session_id });
    if (session == null) throw new ApiError("Invalid session_id", null, HttpStatusCode.UNAUTHORIZED);
    await JWT.verifyToken(session.access_token, "ACCESS");
    let refresh: IRefreshToken | null = await DB.Models.RefreshToken.findOne({ user_id: session.user_id });
    return {
      session_id: session.session_id,
      access_token: session.access_token,
      user_id: session.user_id,
      refresh_token: refresh?.refresh_token,
    };
  }

  /**
   * Get user id by session id or access token
   * @param session_id
   * @param access_token
   */
  public static async getUserIdByTokenOrSession(session_id: string, access_token: string) {
    var session: ISession | null = null;
    if (session_id) session = await DB.Models.Session.findOne({ session_id: session_id });
    else if (access_token) session = await DB.Models.Session.findOne({ access_token: access_token });
    if (!session) return null;
    return session.user_id;
  }
}
