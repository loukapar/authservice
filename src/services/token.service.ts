import ErrorCodes from "../config/error";
import { HttpStatusCode } from "../config/http";
import { IRefreshToken } from "../models/refreshToken";
import DB from "../utils/database";
import ApiError from "../utils/error";
import { JWT } from "../utils/global/jwt";
import { SessionMethods } from "./session.service";
import { UserDetail } from "./user.services/user.detail.service";
import env from "../config/environment";
import { Session } from "inspector";

export interface RefreshTokenBody {
  refresh_token: string;
  session_id: string;
}

interface RefreshTokenResponse {
  access_token: string;
  refresh_token: string;
  session_id: string;
  user_id: string;
}

export class TokenMethods {
  /**
   * Refresh access token of user and recreates refresh_token and session_id
   * @param session_id Session id
   * @param refreshToken Current refresh token
   */
  public static async refreshAccessToken(session_id: string, refreshToken: string): Promise<RefreshTokenResponse> {
    let session = await SessionMethods.getSessionBySessionId(session_id);
    if (!session) {
      console.log(session, session_id);
      await TokenMethods.deleteRefreshTokenIfExists(refreshToken);
      throw new ApiError("Session id not valid", ErrorCodes.SESSION_NOT_VALID, HttpStatusCode.BAD_REQUEST);
    }
    let userID = session.user_id;
    await TokenMethods.refreshTokenExist(refreshToken, userID);
    if (!(await JWT.isTokenValid(refreshToken))) {
      await SessionMethods.logOutUser(userID);
      throw new ApiError("Refresh token not valid", ErrorCodes.REFRESH_TOKEN_NOT_VALID, HttpStatusCode.BAD_REQUEST);
    }
    let user = await UserDetail.getById(userID);
    await SessionMethods.deleteSession(userID);
    let accessToken = await JWT.issueJWT(
      await UserDetail.calculateAccessTokenPayload(user),
      user.email,
      env.ACCESS_TOKEN_EXPIRATION
    );
    let refreshTok = await JWT.issueJWT({}, user.email, env.REFRESH_TOKEN_EXPIRATION);
    await TokenMethods.updateRefreshToken(refreshTok, userID);
    let sessionId = await SessionMethods.storeSession(userID, accessToken);
    return {
      access_token: accessToken,
      refresh_token: refreshTok,
      session_id: sessionId,
      user_id: user._id,
    };
  }

  /**
   * Checks if given refresh token exists in db
   * @param refreshToken refresh token
   * @param userID userId
   */
  private static async refreshTokenExist(refreshToken: string, userID: string) {
    let refreshTokenRecord = await DB.Models.RefreshToken.findOne({ refresh_token: refreshToken, user_id: userID });
    if (!refreshTokenRecord)
      throw new ApiError("Refresh token is not valid", ErrorCodes.REFRESH_TOKEN_NOT_VALID, HttpStatusCode.UNAUTHORIZED);
  }

  /**
   * Stores refresh token in db
   * @param refreshToken Refresh token
   * @param userID userID
   */
  public static async storeRefreshToken(refreshToken: string, userID: string) {
    await DB.Models.RefreshToken.create({
      refresh_token: refreshToken,
      user_id: userID,
    });
  }

  /**
   * Updates refresh token in db
   * @param refreshToken Refresh token
   * @param userID userID
   */
  public static async updateRefreshToken(refreshToken: string, userID: string) {
    await DB.Models.RefreshToken.updateOne({ user_id: userID }, { refresh_token: refreshToken });
  }

  /**
   * Deletes user's refresh token from db
   * @param userID Userid
   */
  public static async deleteRefreshToken(userID: string) {
    await DB.Models.RefreshToken.deleteOne({ user_id: userID });
  }

  public static async deleteRefreshTokenIfExists(refreshToken: string) {
    let refreshTokenInstance = await DB.Models.RefreshToken.findOne({ refresh_token: refreshToken });
    if (refreshTokenInstance) await TokenMethods.deleteRefreshToken(refreshTokenInstance.user_id);
  }

  /**
   * Check if user has active refresh token in db
   * @param userID User id
   */
  public static async checkIfUserHasActiveRefreshToken(userID: string) {
    let refreshToken: IRefreshToken | null = await DB.Models.RefreshToken.findOne({ user_id: userID });
    if (!refreshToken) return null;
    return refreshToken.refresh_token;
  }
}
