import express from "express";
import env from "./config/environment";
import userRoutes from "./routes/user";
import * as bodyParser from "body-parser";
import tokenRoutes from "./routes/token";
import DB from "./utils/database";

/**
 * App Variables
 */
const app: express.Application = express();
const port: string = env.PORT;

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

// parse application/json
app.use(bodyParser.json());

// Allow Access
// Add headers
// app.use(function (req, res, next) {
//   // Request headers you wish to allow
//   if (!PRODUCTION) {
//     const allowedOrigins = ["http://localhost:5000", "http://localhost:5001", "http://localhost:5002"];
//     var origin = req.headers.origin || "undefined";
//     if (allowedOrigins.indexOf(origin) > -1) {
//       res.setHeader("Access-Control-Allow-Origin", origin);
//     }
//     res.setHeader("Access-Control-Allow-Headers", "Content-Type, from");
//     res.setHeader("Access-Control-Allow-Credentials", `${true}`);
//   }
//   next();
// });

/**
 * Routes Definitions
 */
app.get("/", async (req: express.Request, res: express.Response) => {
  res.send("Authentication service is working");
});

app.use((req: express.Request, res: express.Response, next: express.NextFunction) => {
  console.log("Request path:", req.path);
  next();
});

app.use("/oauth/user", userRoutes);
app.use("/oauth/token", tokenRoutes);

/**
 * Server Activation
 */
app.listen(port, async () => {
  DB.Models;
  console.log(`Listening to requests on http://localhost:${port}`);
});
