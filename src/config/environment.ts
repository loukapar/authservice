import * as dotenv from "dotenv";
import path from "path";
// let extIP = require("ext-ip")();
dotenv.config();

function StringToBoolean(value: string): boolean {
  if (value.toUpperCase() === "TRUE") return true;
  return false;
}

export const PRODUCTION = StringToBoolean(process.env.PRODUCTION ?? "FALSE");

const env: EnvironmentVariables = {
  PORT: process.env.PORT ?? "8001",
  // MONGO_URI: process.env.MONGO_URI ?? "",
  SALT_ROUNDS: parseInt(process.env.SALT_ROUNDS || "10"),
  TOKEN_ISSUER: process.env.TOKEN_ISSUER ?? "np.devteam",
  TOKEN_AUDIENCE: process.env.TOKEN_AUDIENCE ?? "cross.com.cy",
  ACCESS_TOKEN_EXPIRATION: process.env.ACCESS_TOKEN_EXPIRATION ?? "2h",
  REFRESH_TOKEN_EXPIRATION: process.env.REFRESH_TOKEN_EXPIRATION ?? "8h",
  VERIFICATION_LINK_TOKEN_EXPIRATION: process.env.VERIFICATION_LINK_TOKEN_EXPIRATION ?? "2d",
  NAME: process.env.NAME ?? "Cross.cy",
  EMAIL_SERVICE: process.env.EMAIL_SERVICE ?? "",
  EMAIL_SERVICE_SENDER: process.env.EMAIL_SERVICE_SENDER ?? "",
  GATEWAY: process.env.GATEWAY ?? "",
  BASE_DIR: path.join(__dirname, ".."),
  CLIENT: `${process.env.CLIENT_WEB_DOMAIN ?? ""}`,
  PHARMACY: `${process.env.PHARMACY_WEB_DOMAIN ?? ""}`,
  ADMIN: `${process.env.ADMIN_WEB_DOMAIN ?? ""}`,
  MONGO_DB_USERNAME: `${process.env.MONGO_INITDB_ROOT_USERNAME ?? ""}`,
  MONGO_DB_PASSWORD: `${process.env.MONGO_INITDB_ROOT_PASSWORD ?? ""}`,
  MONGO_DB_NAME: `${process.env.MONGO_INITDB_DATABASE ?? "pahrmacyauth"}`,
  MONGO_DB_IP: process.env.MONGO_IP ?? "localhost",
};

interface EnvironmentVariables {
  NAME: string;
  PORT: string;
  // MONGO_URI: string;
  SALT_ROUNDS: number;
  TOKEN_ISSUER: string;
  TOKEN_AUDIENCE: string;
  ACCESS_TOKEN_EXPIRATION: string;
  REFRESH_TOKEN_EXPIRATION: string;
  VERIFICATION_LINK_TOKEN_EXPIRATION: string;
  EMAIL_SERVICE: string;
  EMAIL_SERVICE_SENDER: string;
  GATEWAY: string;
  BASE_DIR: string;
  // SERVER_IP?: string;
  // GATEWAY_PORT: string;
  CLIENT?: string;
  PHARMACY?: string;
  ADMIN?: string;
  MONGO_DB_USERNAME: string;
  MONGO_DB_PASSWORD: string;
  MONGO_DB_NAME: string;
  MONGO_DB_IP: string;
}

export default env;
