// ID OF ROLES
export enum UserRoles {
  CLIENT = "CLIENT",
  PHARMACH = "PHARMACY",
  ADMIN = "ADMIN",
}

export enum IUserTypes {
  CLIENT = "CLIENT",
  PHARMACH = "PHARMACY",
  ADMIN = "ADMIN",
}

// USER TYPES DURING REGISTRATION TO DETERMINE ROLE
export const UserType = [IUserTypes.CLIENT, IUserTypes.PHARMACH, IUserTypes.ADMIN];

export const UserTypeRoles = [
  [UserRoles.CLIENT],
  [UserRoles.CLIENT, UserRoles.PHARMACH],
  [UserRoles.CLIENT, UserRoles.PHARMACH, UserRoles.ADMIN],
];
export const AllUserRoles = [UserRoles.CLIENT, UserRoles.PHARMACH, UserRoles.ADMIN];
