import * as express from "express";
import { Schema } from "jsonschema";
import { HttpStatusCode } from "../config/http";
import Response from "../utils/response";
import SchemaValidation from "../utils/schemaValidation";

export default class UserValidators {
  public static updateUserPasswordValidator(req: express.Request, res: express.Response, next: express.NextFunction) {
    try {
      let bodySchema: Schema = {
        type: "object",
        properties: {
          current_password: { type: "string" },
          new_password: { type: "string" },
        },
        required: ["current_password", "new_password"],
        additionalProperties: false,
      };
      SchemaValidation.validate(req.body, bodySchema);
      next();
    } catch (err) {
      return new Response(err._statusCode ?? HttpStatusCode.BAD_REQUEST, null, null, err.message).sendJsonResponse(res);
    }
  }

  public static registerAndLoginValidator(req: express.Request, res: express.Response, next: express.NextFunction) {
    try {
      let bodySchema: Schema = {
        type: "object",
        properties: {
          email: { type: "string" },
          password: { type: "string" },
        },
        required: ["email", "password"],
        additionalProperties: false,
      };
      SchemaValidation.validate(req.body, bodySchema);
      next();
    } catch (err) {
      return new Response(err._statusCode ?? HttpStatusCode.BAD_REQUEST, null, null, err.message).sendJsonResponse(res);
    }
  }

  public static verifyAccountValidator(req: express.Request, res: express.Response, next: express.NextFunction) {
    try {
      let querySchema: Schema = {
        type: "object",
        properties: {
          token: { type: "string" },
          email: { type: "string" },
          client: { type: "string" },
        },
        required: ["token", "email", "client"],
        additionalProperties: false,
      };
      SchemaValidation.validate(req.query, querySchema);
      next();
    } catch (err) {
      return new Response(err._statusCode ?? HttpStatusCode.BAD_REQUEST, null, null, err.message).sendJsonResponse(res);
    }
  }

  public static verifySessionValidator(req: express.Request, res: express.Response, next: express.NextFunction) {
    try {
      let bodySchema: Schema = {
        type: "object",
        properties: {
          session_id: { type: "string" },
        },
        required: ["session_id"],
        additionalProperties: false,
      };
      SchemaValidation.validate(req.body, bodySchema);
      next();
    } catch (err) {
      return new Response(err._statusCode ?? HttpStatusCode.BAD_REQUEST, null, null, err.message).sendJsonResponse(res);
    }
  }

  public static logoutValidator(req: express.Request, res: express.Response, next: express.NextFunction) {
    try {
      let bodySchema: Schema = {
        type: "object",
        properties: {
          session_id: { type: "string" },
          access_token: { type: "string" },
        },
        required: [],
        additionalProperties: false,
      };
      SchemaValidation.validate(req.body, bodySchema);
      next();
    } catch (err) {
      return new Response(err._statusCode ?? HttpStatusCode.BAD_REQUEST, null, null, err.message).sendJsonResponse(res);
    }
  }

  public static forgotPasswordValidator(req: express.Request, res: express.Response, next: express.NextFunction) {
    try {
      let bodySchema: Schema = {
        type: "object",
        properties: {
          email: { type: "string" },
          from: { type: "string" },
        },
        required: ["email", "from"],
        additionalProperties: false,
      };
      SchemaValidation.validate(req.body, bodySchema);
      next();
    } catch (err) {
      return new Response(err._statusCode ?? HttpStatusCode.BAD_REQUEST, null, null, err.message).sendJsonResponse(res);
    }
  }

  public static resetPasswordValidator(req: express.Request, res: express.Response, next: express.NextFunction) {
    try {
      let bodySchema: Schema = {
        type: "object",
        properties: {
          password: { type: "string" },
          confirm_password: { type: "string" },
        },
        required: ["password", "confirm_password"],
        additionalProperties: false,
      };
      SchemaValidation.validate(req.body, bodySchema);
      next();
    } catch (err) {
      return new Response(err._statusCode ?? HttpStatusCode.BAD_REQUEST, null, null, err.message).sendJsonResponse(res);
    }
  }

  public static blockUserValidator(req: express.Request, res: express.Response, next: express.NextFunction) {
    let bodySchema: Schema = {
      type: "object",
      properties: {
        user_id: { type: "string" },
      },
      required: ["user_id"],
      additionalProperties: false,
    };
    SchemaValidation.validate(req.body, bodySchema);
    next();
    try {
    } catch (err) {
      return new Response(err._statusCode ?? HttpStatusCode.BAD_REQUEST, null, null, err.message).sendJsonResponse(res);
    }
  }

  public static socialLoginValidator(req: express.Request, res: express.Response, next: express.NextFunction) {
    try {
      let bodySchema: Schema = {
        type: "object",
        properties: {
          email: { type: "string" },
          token: { type: "string" },
          refreshToken: { type: "string" },
          provider: { type: "string" },
          authid: { type: "string" },
        },
        required: ["email", "token", "refreshToken", "provider", "authid"],
        additionalProperties: false,
      };
      SchemaValidation.validate(req.body, bodySchema);
      next();
    } catch (err) {
      return new Response(err._statusCode ?? HttpStatusCode.BAD_REQUEST, null, null, err.message).sendJsonResponse(res);
    }
  }
}
