import * as express from "express";
import { Schema } from "jsonschema";
import { HttpStatusCode } from "../config/http";
import Response from "../utils/response";
import SchemaValidation from "../utils/schemaValidation";

export default class TokenValidators {
  public static refreshAccessTokenValidator(req: express.Request, res: express.Response, next: express.NextFunction) {
    try {
      let bodySchema: Schema = {
        type: "object",
        properties: {
          refresh_token: { type: "string" },
          session_id: { type: "string" },
        },
        required: ["refresh_token", "session_id"],
        additionalProperties: false,
      };
      SchemaValidation.validate(req.body, bodySchema);
      next();
    } catch (err) {
      return new Response(err._statusCode ?? HttpStatusCode.BAD_REQUEST, null, null, err.message).sendJsonResponse(res);
    }
  }

  public static verifyTokenValidator(req: express.Request, res: express.Response, next: express.NextFunction) {
    try {
      let bodySchema: Schema = {
        type: "object",
        properties: {
          token: { type: "string" },
        },
        required: ["token"],
        additionalProperties: false,
      };
      SchemaValidation.validate(req.body, bodySchema);
      next();
    } catch (err) {
      return new Response(err._statusCode ?? HttpStatusCode.BAD_REQUEST, null, null, err.message).sendJsonResponse(res);
    }
  }

  public static getSessionValidator(req: express.Request, res: express.Response, next: express.NextFunction) {
    try {
      let querySchema: Schema = {
        type: "object",
        properties: {
          session_id: { type: "string" },
        },
        required: ["session_id"],
        additionalProperties: false,
      };
      SchemaValidation.validate(req.query, querySchema);
      next();
    } catch (err) {
      return new Response(err._statusCode ?? HttpStatusCode.BAD_REQUEST, null, null, err.message).sendJsonResponse(res);
    }
  }
}
